package com.teknei.bid.dto;

import java.io.Serializable;

public class BidTipoClienteDTO implements Serializable {
 
	/**
	 * 
	 */
	private static final long serialVersionUID = 3677908808589446127L;
	private String clave;
	private String descripcion;
	 
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}
