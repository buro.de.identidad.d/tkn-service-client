package com.teknei.bid.util;

public enum ClientTypes {

    CLIENT_TYPE_CUSTOMER(1), CLIENT_TYPE_OPERATOR(2);

    ClientTypes(int clientType){
        this.clientType = clientType;
    }

    private int clientType;

    public int getClientTypeValue(){
        return clientType;
    }

}